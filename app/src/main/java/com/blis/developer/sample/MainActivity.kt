package com.blis.developer.sample

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ArrayAdapter
import com.blis.blissdk.BlisSDK
import com.blis.blissdk.annotation.BlisActivity
import com.blis.blissdk.model.LocationEvent
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*

@BlisActivity
class MainActivity : AppCompatActivity(), BlisSDK.BlisSDKListener {
    companion object {
        private const val EVENTS_COUNT = 5
        private const val UPLOAD_INTERVAL = 5000L

        private val DATE_FORMAT = SimpleDateFormat("HH:mm", Locale.getDefault())
    }

    private lateinit var blisSDK: BlisSDK

    private lateinit var adapter: ArrayAdapter<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        blisSDK = BlisSDK.getInstance(application)
        blisSDK.listener = this
        blisSDK.uploadTimeInterval = UPLOAD_INTERVAL
        blisSDK.countFilter = EVENTS_COUNT

        adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, mutableListOf())

        listView.adapter = adapter

        editCountFilter.setText(EVENTS_COUNT.toString())
        editUploadTimeInterval.setText((UPLOAD_INTERVAL / 1000).toString())

        buttonApply.setOnClickListener {
            editUploadTimeInterval.text.toString().toLongOrNull()?.let {
                blisSDK.uploadTimeInterval = it * 1000
            }
            editCountFilter.text.toString().toIntOrNull()?.let {
                blisSDK.countFilter = it
            }
        }

        buttonDebugMode.isChecked = BuildConfig.DEBUG
        buttonDebugMode.setOnCheckedChangeListener { _, isChecked ->
            blisSDK.debugMode = isChecked
        }

        buttonGDPR.setOnClickListener {
            blisSDK.requestGDPR()
        }

        blisSDK.debugMode = BuildConfig.DEBUG
    }

    override fun failedUploadLocations(errorMessage: String?) {
        runOnUiThread {
            adapter.add(errorMessage)
            adapter.notifyDataSetChanged()
        }
    }

    override fun finishUploadLocations(time: Long, list: List<LocationEvent>) {
        runOnUiThread {
            adapter.add("Upload ${list.size} locations at  ${DATE_FORMAT.format(Date(time))}")
            adapter.notifyDataSetChanged()

            listView.setSelection(adapter.count - 1)
        }
    }

}
