package com.blis.blissdk;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.TypedValue;

import com.blis.blissdk.annotation.BlisActivity;
import com.blis.blissdk.model.BlisSDKConfig;
import com.blis.blissdk.model.LocationEvent;
import com.blis.blissdk.model.SubjectToGdpr;
import com.blis.blissdk.model.vendor.Purpose;
import com.blis.blissdk.model.vendor.Vendor;
import com.blis.blissdk.model.vendor.VendorList;
import com.blis.blissdk.storage.CMPStorage;
import com.github.javiersantos.bottomdialogs.BottomDialog;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static android.support.v4.content.ContextCompat.checkSelfPermission;
import static com.blis.blissdk.Utils.equalsToPrecision;

public class BlisSDK extends LocationCallback implements CMPStorage.VendorListCallback,
        BlisAPI.OnUploadListener,
        GAIDRetriever.Callback, LocationEventsCache.ReadCacheCallback,
        ManageDialogBuilder.OnManagingFinishListener,
        Application.ActivityLifecycleCallbacks {
    private static final long LOCATION_REQUEST_INTERVAL_DEBUG = 1_000L;
    private static final long LOCATION_REQUEST_INTERVAL = 60_000L;

    private static final double DEFAULT_PRECISION = 0.0001;

    private static volatile BlisSDK instance = null;

    private boolean isDebugMode = false;

    private String deviceId;

    private volatile long uploadTimeInterval = 300_000;
    private volatile int countFilter = 10;

    private final BlisAPI blisAPI;
    private final CMPStorage cmpStorage;
    private final List<String> euCountryCodes;

    private volatile LocationEvent lastLocation = null;
    private volatile List<LocationEvent> events;

    private final LocationRequest locationRequest;
    private FusedLocationProviderClient fusedLocationProviderClient;

    private Timer timer = null;

    private volatile boolean isLocationTrackingRequested = false;
    private volatile long lastTimeMillis = 0;
    private volatile boolean isRequestingGDPR = false;
    private WeakReference<AlertDialog> gdprDialog = null;

    private volatile boolean isOn = true;
    private volatile SubjectToGdpr subjectToGdpr;

    private WeakReference<BlisSDKListener> weakListener = null;
    private WeakReference<Activity> weakActivity = null;

    private final WeakReference<Context> weakContext;

    private final Handler handler;

    private static void log(@NonNull String message) {
        Log.d(BlisSDK.class.getPackage().getName(), message);
    }

    @NonNull
    @MainThread
    public static BlisSDK getInstance(@NonNull Application application) {
        BlisSDK localInstance = instance;
        if (localInstance == null) {
            synchronized (BlisSDK.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new BlisSDK(application);
                }
            }
        }
        return localInstance;
    }

    private BlisSDK(@NonNull Application application) {
        weakContext = new WeakReference<>(application.getApplicationContext());

        handler = new Handler();

        final BlisSDKConfig config = new BlisSDKConfig(application);

        blisAPI = new BlisAPI(application.getApplicationContext(), config, this);
        cmpStorage = new CMPStorage(application, config);

        euCountryCodes = Arrays.asList(
                application.getResources().getStringArray(R.array.eu_country_codes)
        );

        events = new ArrayList<>();
        locationRequest = LocationRequest.create();

        subjectToGdpr = cmpStorage.getSubjectToGdpr();

        configureLocationRequest(isDebugMode);

        application.registerActivityLifecycleCallbacks(this);
    }

    // region fields access

    public boolean getDebugMode() {
        return isDebugMode;
    }

    public void setDebugMode(boolean isDebugMode) {
        this.isDebugMode = isDebugMode;

        stopLocationUpdates();

        configureLocationRequest(isDebugMode);

        startLocationUpdates();
    }

    public long getUploadTimeInterval() {
        return uploadTimeInterval;
    }

    public void setUploadTimeInterval(long uploadTimeInterval) {
        this.uploadTimeInterval = uploadTimeInterval;

        scheduleTimer();

        uploadLocationUpdatesIfNeeded();
    }

    public int getCountFilter() {
        return countFilter;
    }

    public void setCountFilter(int countFilter) {
        this.countFilter = countFilter;

        uploadLocationUpdatesIfNeeded();
    }

    public void setIsOn(boolean isOn) {
        if (isOn) {
            startLocationUpdates();
        } else {
            stopLocationUpdates();
        }

        this.isOn = isOn;
    }

    public boolean getIsOn() {
        return isOn;
    }

    @Nullable
    public BlisSDKListener getListener() {
        return weakListener.get();
    }

    public void setListener(@Nullable BlisSDKListener listener) {
        if (listener != null) {
            weakListener = new WeakReference<>(listener);
        } else {
            weakListener = null;
        }
    }

    // endregion

    public void requestGDPR() {
        if (isRequestingGDPR()) return;

        final Context context = weakContext.get();

        if (context == null) return;

        isRequestingGDPR = true;

        cmpStorage.getVendorList(context, this);
    }

    private boolean isGDPRRequired() {
        return euCountryCodes.contains(Locale.getDefault().getCountry());
    }

    private boolean isLocationTrackingEnabled() {
        return isOn && (!isGDPRRequired() || subjectToGdpr == SubjectToGdpr.CMPGDPREnabled);
    }

    private void configureLocationRequest(boolean isDebugMode) {
        if (isDebugMode) {
            locationRequest.setInterval(LOCATION_REQUEST_INTERVAL_DEBUG);
            locationRequest.setFastestInterval(LOCATION_REQUEST_INTERVAL_DEBUG);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        } else {
            locationRequest.setInterval(LOCATION_REQUEST_INTERVAL);
            locationRequest.setFastestInterval(LOCATION_REQUEST_INTERVAL);
            locationRequest.setPriority(LocationRequest.PRIORITY_NO_POWER);
        }
    }

    private Boolean isLocationChanged(@NonNull LocationEvent locationEvent) {
        return lastLocation == null || !equalsToPrecision(lastLocation, locationEvent, DEFAULT_PRECISION);
    }

    private void scheduleTimer() {
        if (!isLocationTrackingEnabled() || !isLocationTrackingRequested) return;

        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                uploadLocationUpdatesIfNeeded();
            }
        }, 0, uploadTimeInterval);
    }

    private void uploadLocationUpdatesIfNeeded() {
        if (isLocationTrackingEnabled() && isNeedToUploadLocations()) {
            uploadLocationUpdates();
        }
    }

    private boolean isNeedToUploadLocations() {
        return isLocationsEnoughForUploading() && isUploadTimeIntervalElapsed();
    }

    private boolean isUploadTimeIntervalElapsed() {
        return lastTimeMillis == 0 || (System.currentTimeMillis() - lastTimeMillis) > uploadTimeInterval;
    }

    private boolean isLocationsEnoughForUploading() {
        return events.size() >= countFilter;
    }

    private void uploadLocationUpdates() {
        final List<LocationEvent> eventsToUpload = new ArrayList<>(events);

        events.clear();

        if (deviceId != null) {
            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
                    blisAPI.uploadLocationEvents(eventsToUpload, deviceId);
                }
            });
        } else {
            uploadError("Cannot get deviceId by GAID", eventsToUpload);
        }
    }

    private boolean isRequestingGDPR() {
        return isRequestingGDPR || (gdprDialog != null && gdprDialog.get() != null && gdprDialog.get().isShowing());
    }

    // region GDPR Dialogs

    private void showGDPRDialog(@NonNull final VendorList vendorList) {
        final Context context = weakActivity.get();
        if (context != null) {
            final BottomDialog.Builder builder = new BottomDialog.Builder(context)
                    .setCancelable(false)
                    .setContent(R.string.description_data_collecting)
                    .setPositiveText(R.string.accept)
                    .setNegativeText(R.string.manage)
                    .onPositive(new BottomDialog.ButtonCallback() {
                        @Override
                        public void onClick(@NonNull BottomDialog bottomDialog) {
                            acceptGDPR(vendorList);
                        }
                    })
                    .onNegative(new BottomDialog.ButtonCallback() {
                        @Override
                        public void onClick(@NonNull BottomDialog bottomDialog) {
                            manageGDPR(vendorList);
                        }
                    });

            final Resources.Theme theme = context.getTheme();

            TypedValue v = new TypedValue();

            if (theme.resolveAttribute(R.attr.colorPrimary, v, true)) {
                builder.setPositiveBackgroundColor(v.data);
            }

            if (theme.resolveAttribute(R.attr.colorAccent, v, true)) {
                builder.setNegativeTextColor(v.data);
            }

            builder.show();
        }
    }

    private void acceptGDPR(@NonNull VendorList vendorList) {
        onManagingFinished(vendorList, vendorList.purposes, vendorList.vendors);
    }

    private void manageGDPR(@NonNull VendorList vendorList) {
        isRequestingGDPR = false;

        final Activity activity = weakActivity.get();
        if (activity != null) {
            gdprDialog = new WeakReference<>(
                    new ManageDialogBuilder(vendorList, cmpStorage, this).show(activity)
            );
        }
    }

    private void showGDPRErrorDialog(@NonNull Exception e) {
        final Context context = weakActivity.get();
        if (context != null) {
            new AlertDialog.Builder(context)
                    .setTitle("Error occurred")
                    .setMessage(e.getLocalizedMessage())
                    .setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            isRequestingGDPR = false;
                        }
                    })
                    .show();
        }
    }

    // endregion

    // region CMPStorage.VendorListCallback

    @Override
    public void onVendorListReceived(@NonNull final VendorList vendorList) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                showGDPRDialog(vendorList);
            }
        });
    }

    @Override
    public void onErrorOccurred(@NonNull final Exception e) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                showGDPRErrorDialog(e);
            }
        });
    }

    // endregion

    // region LocationCallback

    @Override
    public void onLocationResult(LocationResult locationResult) {
        final Location location = locationResult.getLastLocation();

        if (isDebugMode) {
            log(String.format(Locale.getDefault(),
                    "Updated location: %f, %f",
                    location.getLatitude(),
                    location.getLongitude()
            ));
        }

        final LocationEvent locationEvent = new LocationEvent(System.currentTimeMillis(),
                location.getLatitude(),
                location.getLongitude(),
                location.getAccuracy(),
                location.getProvider());

        if (isLocationChanged(locationEvent)) {
            lastLocation = locationEvent;

            events.add(locationEvent);

            uploadLocationUpdatesIfNeeded();
        }
    }

    // endregion

    // region BlisAPI.OnUploadListener

    @Override
    public void uploadSuccess(long time, @NonNull List<LocationEvent> events) {
        if (isDebugMode) {
            log("Success locations upload");
        }

        lastTimeMillis = time;

        scheduleTimer();

        final BlisSDKListener listener = weakListener.get();

        if (listener != null) {
            listener.finishUploadLocations(time, events);
        }
    }

    @Override
    public void uploadError(@Nullable String errorMessage, @NonNull List<LocationEvent> events) {
        if (isDebugMode) {
            log(String.format("Upload error message: %s", errorMessage));
        }

        this.events.addAll(events);

        scheduleTimer();

        final BlisSDKListener listener = weakListener.get();

        if (listener != null) {
            listener.failedUploadLocations(errorMessage);
        }
    }

    // endregion

    // region GAIDRetriever.Callback

    @Override
    public void onGAIDRetrieved(@NonNull String deviceId) {
        this.deviceId = deviceId;

        if (isDebugMode) {
            log(String.format("GAIDRetrieved deviceId: %s", deviceId));
        }
    }

    // endregion

    // region LocationEventsCache.ReadCacheCallback

    @Override
    public void onReadFromCache(@NonNull List<LocationEvent> events) {
        this.events.addAll(events);

        if (isDebugMode) {
            log(String.format(Locale.getDefault(), "Read location from cache count: %d", events.size()));
        }

        scheduleTimer();
    }

    // endregion

    // region ManageDialogBuilder.OnManagingFinishListener

    @Override
    public void onManagingFinished(@NonNull VendorList vendorList, @NonNull List<Purpose> purposes, @NonNull List<Vendor> vendors) {
        subjectToGdpr = SubjectToGdpr.CMPGDPREnabled;

        final List<Integer> purposeIds = new ArrayList<>();
        for (Purpose purpose : purposes) {
            purposeIds.add(purpose.id);
        }

        final List<Integer> vendorIds = new ArrayList<>();
        for (Vendor vendor : vendors) {
            vendorIds.add(vendor.id);
        }

        final BlisSDKConfig config = cmpStorage.getBlisSDKConfig();
        cmpStorage.setSubjectToGdpr(subjectToGdpr);
        cmpStorage.setCmpId(config.getCmpId());
        cmpStorage.setCmpVersion(config.getCmpVersion());
        cmpStorage.setConsentScreen(config.getConsetScreen());
        cmpStorage.setConsentLanguage(Locale.getDefault().getLanguage());
        cmpStorage.setVendorListVersion(vendorList.vendorListVersion);
        cmpStorage.setPurposes(purposeIds);
        cmpStorage.setMaxVendorId(vendorIds.isEmpty() ? 0 : Collections.max(vendorIds));
        cmpStorage.setVendors(vendorIds);

        isRequestingGDPR = false;

        startLocationUpdates();
    }

    // endregion

    // region Application.ActivityLifecycleCallbacks

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {
        if (!activity.getClass().isAnnotationPresent(BlisActivity.class)) return;

        weakActivity = new WeakReference<>(activity);

        startLocationUpdates(activity);
    }

    private void startLocationUpdates() {
        if (weakActivity == null) return;

        final Activity activity = weakActivity.get();

        if (activity != null) {
            startLocationUpdates(activity);
        }
    }

    private void startLocationUpdates(@NonNull Activity activity) {
        if (isGDPRRequired() && subjectToGdpr == SubjectToGdpr.CMPGDPRUnknown) {
            requestGDPR();
            return;
        }

        if (!isLocationTrackingEnabled() || isLocationTrackingRequested) return;

        final Context context = activity.getApplicationContext();

        if (isDebugMode) {
            log("Read Cache");
        }

        LocationEventsCache.readFromCache(context, this);

        if (deviceId == null) {
            AsyncTask.execute(new GAIDRetriever(context, this));
        }

        if (fusedLocationProviderClient == null) {
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context);
        }

        final boolean accessFineLocation =
                checkSelfPermission(context, ACCESS_FINE_LOCATION) == PERMISSION_GRANTED;
        final boolean accessCoarseLocation =
                checkSelfPermission(context, ACCESS_COARSE_LOCATION) == PERMISSION_GRANTED;

        if (accessFineLocation && accessCoarseLocation) {
            fusedLocationProviderClient.requestLocationUpdates(locationRequest, this, null);

            if (isDebugMode) {
                log("Start Location Updates");
            }

            isLocationTrackingRequested = true;
        } else {
            PermissionsActivity.start(activity);
        }
    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {
        if (!activity.getClass().isAnnotationPresent(BlisActivity.class)) return;

        stopLocationUpdates();

        weakActivity = null;
    }

    private void stopLocationUpdates() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }

        if (!isLocationTrackingRequested) return;

        if (fusedLocationProviderClient != null) {
            fusedLocationProviderClient.removeLocationUpdates(this);

            if (isDebugMode) {
                log("Stop Location Updates");
            }

            isLocationTrackingRequested = false;
        }

        final Context context = weakContext.get();

        if (context != null && deviceId != null) {
            if (isDebugMode) {
                log("Write Cache");
            }

            LocationEventsCache.writeToCache(context, deviceId, events);
        }
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

    // endregion

    public interface BlisSDKListener {
        void finishUploadLocations(long time, @NonNull List<LocationEvent> events);

        void failedUploadLocations(@Nullable String errorMessage);
    }

}