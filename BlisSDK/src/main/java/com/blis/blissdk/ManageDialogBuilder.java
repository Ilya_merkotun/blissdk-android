package com.blis.blissdk;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.widget.ListView;

import com.blis.blissdk.adapter.checkable.CheckableAdapter;
import com.blis.blissdk.model.vendor.Purpose;
import com.blis.blissdk.model.vendor.Vendor;
import com.blis.blissdk.model.vendor.VendorList;
import com.blis.blissdk.storage.CMPStorage;

import java.lang.ref.WeakReference;
import java.util.List;

public class ManageDialogBuilder implements DialogInterface.OnClickListener, AlertDialog.OnShowListener {


    private final CheckableAdapter<Purpose> purposesAdapter;
    private final CheckableAdapter<Vendor> vendorsAdapter;


    private final VendorList vendorList;

    private final WeakReference<OnManagingFinishListener> weakListener;

    ManageDialogBuilder(@NonNull VendorList vendorList, @NonNull CMPStorage cmpStorage, @NonNull OnManagingFinishListener listener) {
        this.vendorList = vendorList;
        weakListener = new WeakReference<>(listener);

        final List<Integer> selectedPurposes = cmpStorage.getPurposes();
        final List<Integer> selectedVendors = cmpStorage.getVendors();

        purposesAdapter = new CheckableAdapter<>(vendorList.purposes, true);

        if (selectedPurposes.isEmpty()) {
            purposesAdapter.setAllChecked(true, false);
        } else {
            for (Purpose purpose : purposesAdapter.getData()) {
                purposesAdapter.setChecked(purpose, selectedPurposes.contains(purpose.id), false);
            }
        }

        vendorsAdapter = new CheckableAdapter<>(vendorList.vendors, true);

        if (selectedVendors.isEmpty()) {
            vendorsAdapter.setAllChecked(true, false);
        } else {
            for (Vendor vendor : vendorsAdapter.getData()) {
                vendorsAdapter.setChecked(vendor, selectedVendors.contains(vendor.id), false);
            }
        }
    }

    @NonNull
    public AlertDialog show(@NonNull Activity activity) {
        final AlertDialog dialog = new AlertDialog.Builder(activity)
                .setView(R.layout.dialog_manage)
                .setPositiveButton(R.string.i_have_finished, this)
                .setCancelable(false)
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                    }
                }).create();

        dialog.setOnShowListener(this);
        dialog.show();

        return dialog;
    }

    //
    @Override
    public void onClick(DialogInterface dialog, int which) {
        final OnManagingFinishListener listener = weakListener.get();

        if (listener == null) return;

        final List<Purpose> checkedPurposes = purposesAdapter.getCheckedItems();
        final List<Vendor> checkedVendors = vendorsAdapter.getCheckedItems();

        listener.onManagingFinished(vendorList, checkedPurposes, checkedVendors);
    }

    @Override
    public void onShow(DialogInterface dialog) {
        final AlertDialog alertDialog = (AlertDialog) dialog;

        final ListView purposesList = alertDialog.findViewById(R.id.list_purposes);
        if (purposesList != null) {
            purposesList.setAdapter(purposesAdapter);
        }

        final ListView vendorsList = alertDialog.findViewById(R.id.list_partners);
        if (vendorsList != null) {
            vendorsList.setAdapter(vendorsAdapter);
        }
    }

    public interface OnManagingFinishListener {
        void onManagingFinished(@NonNull VendorList vendorList,
                                @NonNull List<Purpose> purposes,
                                @NonNull List<Vendor> vendors);
    }
}
