package com.blis.blissdk.model.vendor;

import android.support.annotation.NonNull;

import com.blis.blissdk.adapter.checkable.CheckableItem;
import com.blis.blissdk.model.Identifiable;
import com.blis.blissdk.model.Key;
import com.blis.blissdk.util.json.FromJSONObjectCreator;
import com.blis.blissdk.util.json.JSONObjectModel;

import org.json.JSONException;
import org.json.JSONObject;

public class Purpose implements Identifiable, CheckableItem, JSONObjectModel {
    public final int id;
    public final String name;
    public final String description;

    private Purpose(@NonNull JSONObject json) throws JSONException {
        id = json.getInt(Key.ID);
        name = json.getString(Key.NAME);
        description = json.getString(Key.DESCRIPTION);
    }

    @Override
    public int getId() {
        return id;
    }

    @NonNull
    @Override
    public String getTitle() {
        return name;
    }

    @NonNull
    @Override
    public JSONObject toJSONObject() throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put(Key.ID, id);
        jsonObject.put(Key.NAME, name);
        jsonObject.put(Key.DESCRIPTION, description);
        return jsonObject;
    }

    public static final FromJSONObjectCreator<Purpose> JSON_OBJECT_CREATOR = new FromJSONObjectCreator<Purpose>() {
        @NonNull
        @Override
        public Purpose fromJSONObject(@NonNull JSONObject json) throws JSONException {
            return new Purpose(json);
        }
    };
}
