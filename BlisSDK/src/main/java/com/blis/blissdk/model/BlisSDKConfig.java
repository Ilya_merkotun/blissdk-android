package com.blis.blissdk.model;

import android.content.Context;
import android.support.annotation.NonNull;

import com.blis.blissdk.util.InputStreamUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class BlisSDKConfig {
    private static final String FILENAME = "BlisSDKConfig.json";
    private static final String VENDOR_LIST_URL = "https://vendorlist.consensu.org/vendorlist.json";
    private static final int DEFAULT_CMP_ID = 1;
    private static final int DEFAULT_CMP_VERSION = 1;
    private static final int DEFAULT_CONSENT_SCREEN = 2;

    private static final String KEY_URL = "URL";
    private static final String KEY_API_VERSION = "APIVersion";
    private static final String KEY_CMP_ID = "CmpId";
    private static final String KEY_CMP_VERSION = "CmpVersion";
    private static final String KEY_CONSENT_SCREEN = "ConsentScreen";
    private static final String KEY_VENDOR_LIST_URL = "VendorListUrl";

    private final String url;
    private final String apiVersion;
    private final int cmpId;
    private final int cmpVersion;
    private final int consetScreen;
    private final String vendorListUrl;

    public BlisSDKConfig(@NonNull Context context) {
        try {
            final InputStream inputStream = context.getResources().getAssets().open(FILENAME);

            if (inputStream != null) {
                final String fileString = InputStreamUtils.readStreamAsString(inputStream);

                try {
                    final JSONObject jsonObject = new JSONObject(fileString);

                    final String url = jsonObject.optString(KEY_URL);
                    if (url != null) {
                        this.url = url;
                    } else {
                        throw new RuntimeException(String.format("Missing api URL value for key %s.", KEY_URL));
                    }

                    final String apiVersion = jsonObject.optString(KEY_API_VERSION);
                    if (apiVersion != null) {
                        this.apiVersion = apiVersion;
                    } else {
                        throw new RuntimeException(String.format("Missing api version value for key %s.", KEY_API_VERSION));
                    }

                    cmpId = jsonObject.optInt(KEY_CMP_ID, DEFAULT_CMP_ID);
                    cmpVersion = jsonObject.optInt(KEY_CMP_VERSION, DEFAULT_CMP_VERSION);
                    consetScreen = jsonObject.optInt(KEY_CONSENT_SCREEN, DEFAULT_CONSENT_SCREEN);

                    vendorListUrl = jsonObject.optString(KEY_VENDOR_LIST_URL, VENDOR_LIST_URL);

                    return;

                } catch (JSONException ignored) {
                    throw new RuntimeException(String.format("%s has wrong format can't be parsed.", FILENAME));
                }
            }
        } catch (IOException ignored) {
        }

        throw new RuntimeException(String.format("%s doesn't exists.", FILENAME));
    }

    @NonNull
    public String getUrl() {
        return url;
    }

    @NonNull
    public String getApiVersion() {
        return apiVersion;
    }

    public int getCmpId() {
        return cmpId;
    }

    public int getCmpVersion() {
        return cmpVersion;
    }

    public int getConsetScreen() {
        return consetScreen;
    }

    @NonNull
    public String getVendorListUrl() {
        return vendorListUrl;
    }

    public String getRequestUrl() {
        return url + "/v" + apiVersion + "/api/publisher/location";
    }
}
