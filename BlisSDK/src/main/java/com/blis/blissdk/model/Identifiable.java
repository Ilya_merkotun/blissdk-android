package com.blis.blissdk.model;

public interface Identifiable {
    int getId();
}
