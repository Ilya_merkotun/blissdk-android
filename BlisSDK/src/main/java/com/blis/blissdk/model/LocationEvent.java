package com.blis.blissdk.model;

import org.json.JSONException;
import org.json.JSONObject;

public class LocationEvent {
    private long timestamp;
    private double latitude;
    private double longitude;
    private double accuracy;
    private String locationProvider;

    public LocationEvent(long timestamp, double latitude, double longitude, double accuracy, String locationProvider) {
        this.timestamp = timestamp;
        this.latitude = latitude;
        this.longitude = longitude;
        this.accuracy = accuracy;
        this.locationProvider = locationProvider;
    }

    public LocationEvent(JSONObject jsonObject) throws JSONException {
        timestamp = jsonObject.getLong("timestamp");
        latitude = jsonObject.getDouble("latitude");
        longitude = jsonObject.getDouble("longitude");
        accuracy = jsonObject.getDouble("accuracy");
        locationProvider = jsonObject.getString("locationProvider");
    }

    public long getTimestamp() {
        return timestamp;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public String getLocationProvider() {
        return locationProvider;
    }

    public JSONObject toJSONObject(String deviceId, String bundleId) throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("timestamp", timestamp);
        jsonObject.put("deviceId", deviceId);
        jsonObject.put("bundleId", bundleId);
        jsonObject.put("latitude", latitude);
        jsonObject.put("longitude", longitude);
        jsonObject.put("accuracy", accuracy);
        jsonObject.put("locationProvider", locationProvider);

        return jsonObject;
    }
}