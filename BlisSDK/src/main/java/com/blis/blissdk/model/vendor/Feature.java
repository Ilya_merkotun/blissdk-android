package com.blis.blissdk.model.vendor;

import android.support.annotation.NonNull;

import com.blis.blissdk.model.Key;
import com.blis.blissdk.util.json.FromJSONObjectCreator;
import com.blis.blissdk.util.json.JSONObjectModel;

import org.json.JSONException;
import org.json.JSONObject;

public class Feature implements JSONObjectModel {
    public final long id;
    public final String name;
    public final String description;

    private Feature(@NonNull JSONObject json) throws JSONException {
        id = json.getLong(Key.ID);
        name = json.getString(Key.NAME);
        description = json.getString(Key.DESCRIPTION);
    }

    @NonNull
    @Override
    public JSONObject toJSONObject() throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put(Key.ID, id);
        jsonObject.put(Key.NAME, name);
        jsonObject.put(Key.DESCRIPTION, description);
        return jsonObject;
    }

    public static final FromJSONObjectCreator<Feature> JSON_OBJECT_CREATOR = new FromJSONObjectCreator<Feature>() {
        @NonNull
        @Override
        public Feature fromJSONObject(@NonNull JSONObject json) throws JSONException {
            return new Feature(json);
        }
    };
}
