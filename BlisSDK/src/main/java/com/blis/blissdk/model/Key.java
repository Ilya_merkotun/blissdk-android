package com.blis.blissdk.model;

public interface Key {
    String ID = "id";
    String NAME = "name";
    String DESCRIPTION = "description";
    String PURPOSES = "purposes";
    String FEATURES = "features";
    String VENDORS = "vendors";
    String LAST_UPDATE = "lastUpdated";
    String VENDOR_LIST_VERSION = "vendorListVersion";
    String POLICY_URL = "policyUrl";
    String PURPOSE_IDS = "purposeIds";
    String LEG_INT_PURPOSE_IDS = "legIntPurposeIds";
    String FEATURE_IDS = "featureIds";
}
