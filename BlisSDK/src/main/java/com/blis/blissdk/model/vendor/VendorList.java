package com.blis.blissdk.model.vendor;

import android.support.annotation.NonNull;

import com.blis.blissdk.model.Key;
import com.blis.blissdk.util.date.DateUtils;
import com.blis.blissdk.util.json.JSONObjectModel;
import com.blis.blissdk.util.json.JSONUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import static com.blis.blissdk.util.json.JSONUtils.list;

public class VendorList implements JSONObjectModel {
    public final int vendorListVersion;
    public final Date lastUpdate;
    public final List<Purpose> purposes;
    public final List<Feature> features;
    public final List<Vendor> vendors;

    public VendorList(@NonNull JSONObject json) throws JSONException, ParseException {
        vendorListVersion = json.getInt(Key.VENDOR_LIST_VERSION);
        lastUpdate = DateUtils.FORMAT.parse(json.getString(Key.LAST_UPDATE));
        purposes = list(json.optJSONArray(Key.PURPOSES), Purpose.JSON_OBJECT_CREATOR);
        features = list(json.optJSONArray(Key.FEATURES), Feature.JSON_OBJECT_CREATOR);
        vendors = list(json.optJSONArray(Key.VENDORS), Vendor.JSON_OBJECT_CREATOR);
    }

    @NonNull
    @Override
    public JSONObject toJSONObject() throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put(Key.VENDOR_LIST_VERSION, vendorListVersion);
        jsonObject.put(Key.LAST_UPDATE, DateUtils.FORMAT.format(lastUpdate));
        jsonObject.put(Key.PURPOSES, JSONUtils.array(purposes));
        jsonObject.put(Key.FEATURES, JSONUtils.array(features));
        jsonObject.put(Key.VENDORS, JSONUtils.array(vendors));
        return jsonObject;
    }
}
