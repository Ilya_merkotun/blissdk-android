package com.blis.blissdk.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Enum that indicates:
 * 'CMPGDPRDisabled' – value 0, not subject to GDPR,
 * 'CMPGDPREnabled'  – value 1, subject to GDPR,
 * 'CMPGDPRUnknown'  - value -1, unset.
 */
public enum SubjectToGdpr {
    CMPGDPRUnknown("-1"), CMPGDPRDisabled("0"), CMPGDPREnabled("1");
    private final String value;

    SubjectToGdpr(String value) {
        this.value = value;
    }

    @NonNull
    public static SubjectToGdpr getValueForString(@Nullable final String valueString) {
        for (SubjectToGdpr value : values()) {
            if (value.value.equals(valueString)) {
                return value;
            }
        }
        return CMPGDPRUnknown;
    }

    public String getValue() {
        return value;
    }
}
