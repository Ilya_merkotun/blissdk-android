package com.blis.blissdk.model.vendor;

import android.support.annotation.NonNull;

import com.blis.blissdk.adapter.checkable.CheckableItem;
import com.blis.blissdk.model.Identifiable;
import com.blis.blissdk.model.Key;
import com.blis.blissdk.util.json.FromJSONObjectCreator;
import com.blis.blissdk.util.json.JSONObjectModel;
import com.blis.blissdk.util.json.JSONUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static com.blis.blissdk.util.json.JSONUtils.list;

public class Vendor implements Identifiable, CheckableItem, JSONObjectModel {
    public final int id;
    public final String name;
    public final String policyUrl;
    public final List<Integer> purposeIds;
    public final List<Integer> legIntPurposeIds;
    public final List<Integer> featureIds;

    private Vendor(@NonNull JSONObject json) throws JSONException {
        id = json.getInt(Key.ID);
        name = json.getString(Key.NAME);
        policyUrl = json.getString(Key.POLICY_URL);
        purposeIds = list(json.getJSONArray(Key.PURPOSE_IDS));
        legIntPurposeIds = list(json.getJSONArray(Key.LEG_INT_PURPOSE_IDS));
        featureIds = list(json.getJSONArray(Key.FEATURE_IDS));
    }

    @Override
    public int getId() {
        return id;
    }

    @NonNull
    @Override
    public String getTitle() {
        return name;
    }

    @NonNull
    @Override
    public JSONObject toJSONObject() throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put(Key.ID, id);
        jsonObject.put(Key.NAME, name);
        jsonObject.put(Key.POLICY_URL, policyUrl);
        jsonObject.put(Key.PURPOSE_IDS, JSONUtils.array(purposeIds));
        jsonObject.put(Key.LEG_INT_PURPOSE_IDS, JSONUtils.array(legIntPurposeIds));
        jsonObject.put(Key.FEATURE_IDS, JSONUtils.array(featureIds));
        return jsonObject;
    }

    public static final FromJSONObjectCreator<Vendor> JSON_OBJECT_CREATOR = new FromJSONObjectCreator<Vendor>() {
        @NonNull
        @Override
        public Vendor fromJSONObject(@NonNull JSONObject json) throws JSONException {
            return new Vendor(json);
        }
    };
}
