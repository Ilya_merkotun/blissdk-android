package com.blis.blissdk;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.blis.blissdk.model.LocationEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

class LocationEventsCache {
    private final static String FILE_NAME = "BlisSDKCache.json";

    static void readFromCache(final Context context, final ReadCacheCallback callback) {
        AsyncTask.execute(new Runnable() {
            final WeakReference<ReadCacheCallback> callbackWeakReference = new WeakReference<>(callback);

            @Override
            public void run() {
                final ReadCacheCallback callback = callbackWeakReference.get();
                if (callback != null) {
                    callback.onReadFromCache(readEventsFromFile(context));
                }

            }
        });
    }

    static void writeToCache(@NonNull final Context context, @NonNull final String deviceId, @NonNull final List<LocationEvent> locationEvents) {
        AsyncTask.execute(new Runnable() {
            final WeakReference<Context> contextWeakReference = new WeakReference<>(context);

            @Override
            public void run() {
                final Context context = contextWeakReference.get();

                if (context == null) {
                    return;
                }

                try {
                    final FileOutputStream fos = context.openFileOutput(FILE_NAME, MODE_PRIVATE);
                    final OutputStreamWriter osWriter = new OutputStreamWriter(fos);

                    final JSONArray eventsJSON = new JSONArray();
                    for (LocationEvent locationEvent : locationEvents) {
                        eventsJSON.put(locationEvent.toJSONObject(deviceId, context.getPackageName()));
                    }

                    osWriter.write(eventsJSON.toString());
                    osWriter.flush();
                    osWriter.close();

                } catch (IOException | JSONException e) {
                    Log.e("Exception", "File write failed: " + e.toString());
                }
            }
        });
    }

    @NonNull
    private static List<LocationEvent> readEventsFromFile(@NonNull Context context) {
        final List<LocationEvent> locationEvents = new ArrayList<>();

        try {
            final File file = context.getFileStreamPath(FILE_NAME);

            if (!file.exists()) return locationEvents;

            final InputStream inputStream = context.openFileInput(FILE_NAME);

            if (inputStream != null) {
                final InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                final StringBuilder stringBuilder = new StringBuilder();

                String receiveString;

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();

                final String jsonString = stringBuilder.toString();
                final JSONArray jsonArray = new JSONArray(jsonString);

                for (int index = 0; index < jsonArray.length(); index++) {
                    final JSONObject eventJSON = jsonArray.getJSONObject(index);

                    locationEvents.add(new LocationEvent(eventJSON));
                }
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        return locationEvents;
    }

    interface ReadCacheCallback {
        void onReadFromCache(@NonNull List<LocationEvent> locationEvents);
    }
}