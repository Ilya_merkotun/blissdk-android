package com.blis.blissdk.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

public abstract class DataAdapter<T, H extends ViewHolder> extends BaseAdapter {

    private final List<T> data;

    public DataAdapter(@Nullable List<T> data) {
        this.data = data == null ? new ArrayList<T>() : new ArrayList<>(data);
    }

    public void setData(@Nullable List<T> data) {
        this.data.clear();

        if (data != null && data.size() > 0) {
            this.data.addAll(data);
        }
    }

    @NonNull
    public List<T> getData() {
        return data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @NonNull
    @Override
    public T getItem(int position) {
        return data.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final H holder;
        if (convertView == null) {
            holder = onCreateViewHolder(parent, position);
            holder.itemView.setTag(holder);
        } else {
            //noinspection unchecked
            holder = (H) convertView.getTag();
        }

        holder.adapterPosition = position;

        onBindViewHolder(holder, position, getItem(position));

        return holder.itemView;
    }

    @NonNull
    protected abstract H onCreateViewHolder(ViewGroup parent, int position);

    protected abstract void onBindViewHolder(@NonNull H holder, int position, @NonNull T item);
}
