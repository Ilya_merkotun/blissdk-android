package com.blis.blissdk.adapter.checkable;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.ViewGroup;

import com.blis.blissdk.adapter.DataAdapter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CheckableAdapter<T extends CheckableItem> extends DataAdapter<T, CheckableViewHolder>
        implements CheckableViewHolder.OnViewHolderCheckListener {

    private final Set<Integer> checkedItemPositions = new HashSet<>();

    public CheckableAdapter() {
        this(null, false);
    }

    public CheckableAdapter(@Nullable List<T> data, boolean isChecked) {
        super(data);

        if (isChecked) setAllChecked(true, false);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public void setData(@Nullable List<T> data, boolean isChecked) {
        super.setData(data);
        setAllChecked(isChecked, false);
    }

    public void setChecked(@NonNull List<T> items, boolean isChecked, boolean notify) {
        for (T item : items) {
            setChecked(item, isChecked, false);
        }

        if (notify) {
            notifyDataSetChanged();
        }
    }

    public void setChecked(@NonNull T item, boolean isChecked, boolean notify) {
        final int position = getData().indexOf(item);
        if (position >= 0) {
            if (isChecked) {
                checkedItemPositions.add(position);
            } else {
                checkedItemPositions.remove(position);
            }
        }

        if (notify) {
            notifyDataSetChanged();
        }
    }

    public void setAllChecked(boolean isChecked, boolean notify) {
        if (isChecked) {
            for (int position = 0; position < getCount(); position++) {
                checkedItemPositions.add(position);
            }

        } else if (!checkedItemPositions.isEmpty()) {
            checkedItemPositions.clear();
        }

        if (notify) {
            notifyDataSetChanged();
        }
    }

    @NonNull
    public List<T> getCheckedItems() {
        final List<T> items = new ArrayList<>(checkedItemPositions.size());

        for (Integer position : checkedItemPositions) {
            items.add(getItem(position));
        }

        return items;
    }

    @NonNull
    @Override
    protected CheckableViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        return new CheckableViewHolder(parent, this);
    }

    @Override
    protected void onBindViewHolder(@NonNull CheckableViewHolder holder, int position, @NonNull T item) {
        holder.setTitle(item.getTitle());
        holder.setChecked(checkedItemPositions.contains(position));
    }

    @Override
    public void onViewHolderChecked(@NonNull CheckableViewHolder holder, boolean isChecked) {
        if (isChecked) {
            checkedItemPositions.add(holder.adapterPosition);
        } else {
            checkedItemPositions.remove(holder.adapterPosition);
        }
    }
}
