package com.blis.blissdk.adapter.checkable;

import android.support.annotation.NonNull;

public interface CheckableItem {
    @NonNull
    String getTitle();
}
