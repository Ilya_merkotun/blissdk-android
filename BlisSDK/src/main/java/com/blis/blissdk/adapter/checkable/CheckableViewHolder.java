package com.blis.blissdk.adapter.checkable;

import android.support.annotation.NonNull;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.blis.blissdk.R;
import com.blis.blissdk.adapter.ViewHolder;

public class CheckableViewHolder extends ViewHolder implements CompoundButton.OnCheckedChangeListener {

    private final CheckBox checkBox;

    private final OnViewHolderCheckListener listener;

    public CheckableViewHolder(@NonNull ViewGroup parent, @NonNull OnViewHolderCheckListener listener) {
        super(parent, R.layout.item_check_box_1);

        this.listener = listener;

        checkBox = itemView.findViewById(R.id.check_box_1);

        checkBox.setOnCheckedChangeListener(this);
    }

    public void setTitle(@NonNull String title) {
        checkBox.setText(title);
    }

    public void setChecked(boolean isChecked) {
        checkBox.setChecked(isChecked);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        listener.onViewHolderChecked(this, isChecked);
    }

    public interface OnViewHolderCheckListener {
        void onViewHolderChecked(@NonNull CheckableViewHolder holder, boolean isChecked);
    }
}
