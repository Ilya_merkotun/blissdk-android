package com.blis.blissdk;

import com.blis.blissdk.model.LocationEvent;

class Utils {

    static Boolean equalsToPrecision(Double first, Double second, Double precision) {
        return Math.abs(Math.abs(first) - Math.abs(second)) < precision;
    }

    static Boolean equalsToPrecision(LocationEvent first, LocationEvent second, Double precision) {
        return equalsToPrecision(first.getLatitude(), second.getLatitude(), precision)
                && equalsToPrecision(first.getLongitude(), second.getLongitude(), precision);
    }


}