package com.blis.blissdk;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;

import com.blis.blissdk.model.BlisSDKConfig;
import com.blis.blissdk.model.LocationEvent;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Locale;

class BlisAPI {
    private final String packageName;

    private WeakReference<OnUploadListener> weakListener;
    private BlisSDKConfig blisSDKConfig;

    BlisAPI(@NonNull Context context, @NonNull BlisSDKConfig config, @NonNull OnUploadListener listener) {
        packageName = context.getPackageName();
        weakListener = new WeakReference<>(listener);
        blisSDKConfig = config;
    }

    @WorkerThread
    void uploadLocationEvents(@NonNull List<LocationEvent> locationEvents, @NonNull String deviceId) {
        final OnUploadListener listener = weakListener.get();

        try {
            final long time = System.currentTimeMillis();

            final URL url = new URL(blisSDKConfig.getRequestUrl());

            final HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
            httpConn.setUseCaches(false);
            httpConn.setDoInput(true);
            httpConn.setRequestMethod("POST");

            final JSONArray jsonArray = new JSONArray();
            for (LocationEvent locationEvent : locationEvents) {
                jsonArray.put(locationEvent.toJSONObject(deviceId, packageName));
            }

            final OutputStreamWriter writer = new OutputStreamWriter(httpConn.getOutputStream());
            writer.write(jsonArray.toString());
            writer.flush();
            writer.close();

            final int responseCode = httpConn.getResponseCode();

            httpConn.disconnect();

            if (listener == null) return;

            if (responseCode == 204) {
                listener.uploadSuccess(time, locationEvents);
            } else {
                listener.uploadError(String.format(Locale.getDefault(), "Fail to upload location events with response code %d", responseCode), locationEvents);
            }

        } catch (IOException | JSONException e) {
            e.printStackTrace();

            if (listener != null) {
                listener.uploadError(e.getMessage(), locationEvents);
            }
        }
    }

    interface OnUploadListener {
        void uploadSuccess(long time, @NonNull List<LocationEvent> events);

        void uploadError(@Nullable String errorMessage, @NonNull List<LocationEvent> events);
    }
}