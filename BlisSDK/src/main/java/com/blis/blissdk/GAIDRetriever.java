package com.blis.blissdk;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;

import java.lang.ref.WeakReference;

class GAIDRetriever implements Runnable {
    private final WeakReference<Context> contextWeakReference;
    private final WeakReference<Callback> callbackWeakReference;

    GAIDRetriever(Context context, Callback callback) {
        contextWeakReference = new WeakReference<>(context);
        callbackWeakReference = new WeakReference<>(callback);
    }

    @Override
    public void run() {
        final Context context = contextWeakReference.get();
        if (context != null) {
            try {
                final AdvertisingIdClient.Info info = AdvertisingIdClient.getAdvertisingIdInfo(context);

                final Callback callback = callbackWeakReference.get();
                if (callback != null) {
                    callback.onGAIDRetrieved(info.getId());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public interface Callback {
        void onGAIDRetrieved(@NonNull String deviceId);
    }
}
