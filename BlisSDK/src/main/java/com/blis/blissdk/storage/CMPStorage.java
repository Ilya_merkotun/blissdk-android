package com.blis.blissdk.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;

import com.blis.blissdk.model.BlisSDKConfig;
import com.blis.blissdk.model.SubjectToGdpr;
import com.blis.blissdk.model.vendor.VendorList;
import com.blis.blissdk.util.InputStreamUtils;
import com.blis.blissdk.util.PreferenceUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.List;

/**
 * Used to retrieve and store the consentData, subjectToGdpr, vendors and purposes in the SharedPreferences
 */
public class CMPStorage {

    private static final int DEFAULT_CMP_ID = 1;

    private static final String VENDOR_LIST_FILE_NAME = "vendorlist.json";

    private static final String SUBJECT_TO_GDPR = "IABConsent_SubjectToGDPR";
    private static final String CMP_ID = "IABConsent_CmpId";
    private static final String CMP_VERSION = "IABConsent_CmpVersion";
    private static final String CONSENT_SCREEN = "IABConsent_ConsentScreen";
    private static final String CONSENT_LANGUAGE = "IABConsent_ConsentLanguage";
    private static final String VENDOR_LIST_VERSION = "IABConsent_VendorListVersion";
    private static final String MAX_VENDOR_ID = "IABConsent_MAX_VENDOR_ID";
    private static final String VENDORS = "IABConsent_ParsedVendorConsents";
    private static final String PURPOSES = "IABConsent_ParsedPurposeConsents";

    private SubjectToGdpr subjectToGdpr;
    private Integer cmpId;
    private Integer cmpVersion;
    private Integer consentScreen;
    private String consentLanguage;
    private Integer vendorListVersion;
    private Integer macVendorId;
    private List<Integer> vendors;
    private List<Integer> purposes;

    private VendorList vendorList;

    private final SharedPreferences preferences;
    private final BlisSDKConfig blisSDKConfig;

    public CMPStorage(@NonNull Context context, @NonNull BlisSDKConfig config) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.registerOnSharedPreferenceChangeListener(preferenceChangeListener);

        blisSDKConfig = config;
    }

    @NonNull
    public BlisSDKConfig getBlisSDKConfig() {
        return blisSDKConfig;
    }

    @NonNull
    @WorkerThread
    private VendorList getVendorList(@NonNull Context context) {
        final VendorList shippedVendorList;
        try {
            final InputStream inputStream = context.getAssets().open(VENDOR_LIST_FILE_NAME);

            if (inputStream != null) {
                shippedVendorList = InputStreamUtils.readVendorList(inputStream);

            } else {
                throw new IllegalArgumentException("%s doesn't exists in assets directory.");
            }

        } catch (IOException | JSONException | ParseException e) {
            throw new IllegalArgumentException(e);
        }

        VendorList storedVendorList = null;
        if (vendorList == null) {
            try {
                if (context.getFileStreamPath(VENDOR_LIST_FILE_NAME).exists()) {
                    final InputStream inputStream = context.openFileInput(VENDOR_LIST_FILE_NAME);
                    if (inputStream != null) {
                        storedVendorList = InputStreamUtils.readVendorList(inputStream);
                    }
                }

            } catch (IOException | JSONException | ParseException e) {
                storedVendorList = null;
            }
        } else {
            storedVendorList = vendorList;
        }

        VendorList freshVendorList;
        try {
            final URL url = new URL(blisSDKConfig.getVendorListUrl());
            final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setUseCaches(false);

            freshVendorList = InputStreamUtils.readVendorList(connection.getInputStream());

            connection.disconnect();

        } catch (IOException | ParseException | JSONException e) {
            freshVendorList = null;
        }

        if (freshVendorList != null) {
            if (storedVendorList == null ||
                    freshVendorList.vendorListVersion > storedVendorList.vendorListVersion) {
                try {
                    final OutputStream outputStream =
                            context.openFileOutput(VENDOR_LIST_FILE_NAME, Context.MODE_PRIVATE);
                    final JSONObject vendorListJSON = freshVendorList.toJSONObject();
                    final String vendorListJSONString = vendorListJSON.toString();

                    outputStream.write(vendorListJSONString.getBytes());
                    outputStream.flush();
                    outputStream.close();

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }

                vendorList = freshVendorList;

            } else {
                vendorList = storedVendorList;
            }

        } else if (storedVendorList != null) {
            if (storedVendorList.vendorListVersion >= shippedVendorList.vendorListVersion) {
                vendorList = storedVendorList;
            } else {
                vendorList = shippedVendorList;

                try {
                    //noinspection ResultOfMethodCallIgnored
                    context.getFileStreamPath(VENDOR_LIST_FILE_NAME).delete();
                } catch (SecurityException ignored) {
                }
            }

        } else {
            vendorList = shippedVendorList;
        }

        return vendorList;
    }

    public void getVendorList(@NonNull final Context context, @NonNull final VendorListCallback callback) {
        AsyncTask.execute(new Runnable() {
            final WeakReference<VendorListCallback> weakCallback = new WeakReference<>(callback);

            @Override
            public void run() {
                try {
                    final VendorList vendorList = getVendorList(context);
                    final VendorListCallback callback = weakCallback.get();
                    if (callback != null) {
                        callback.onVendorListReceived(vendorList);
                    }
                } catch (final Exception e) {
                    final VendorListCallback callback = weakCallback.get();
                    if (callback != null) {
                        callback.onErrorOccurred(e);
                    }
                }

            }
        });
    }

    /**
     * Returns the {@link SubjectToGdpr} stored in the SharedPreferences
     *
     * @return the stored {@link SubjectToGdpr}
     */
    @NonNull
    public SubjectToGdpr getSubjectToGdpr() {
        if (subjectToGdpr == null) {
            subjectToGdpr = SubjectToGdpr.getValueForString(
                    preferences.getString(SUBJECT_TO_GDPR, null)
            );
        }

        return subjectToGdpr;
    }

    /**
     * Stores the passed {@link SubjectToGdpr} in the SharedPreferences
     *
     * @param subjectToGdpr the {@link SubjectToGdpr} to be stored
     */
    public void setSubjectToGdpr(@NonNull SubjectToGdpr subjectToGdpr) {
        this.subjectToGdpr = subjectToGdpr;

        String subjectToGdprValue = null;

        if (subjectToGdpr == SubjectToGdpr.CMPGDPRDisabled || subjectToGdpr == SubjectToGdpr.CMPGDPREnabled) {
            subjectToGdprValue = subjectToGdpr.getValue();
        }

        preferences.edit().putString(SUBJECT_TO_GDPR, subjectToGdprValue).apply();
    }


    /**
     * Returns the CMP id stored in the SharedPreferences
     *
     * @return the stored CMP id
     */
    public int getCmpId() {
        if (cmpId == null) {
            cmpId = preferences.getInt(CMP_ID, DEFAULT_CMP_ID);
        }
        return cmpId;
    }

    /**
     * Stores the passed CMP id in the SharedPreferences
     *
     * @param cmpId the CMP id to be stored
     */
    public void setCmpId(int cmpId) {
        this.cmpId = cmpId;

        preferences.edit().putInt(CMP_ID, cmpId).apply();
    }

    /**
     * Returns the CMP version stored in the SharedPreferences
     *
     * @return the stored CMP version
     */
    public int getCmpVersion() {
        if (cmpVersion == null) {
            // TODO: Version of the stored cmp.
            cmpVersion = preferences.getInt(CMP_VERSION, -1);
        }
        return cmpVersion;
    }

    /**
     * Stores the passed CMP version in the SharedPreferences
     *
     * @param cmpVersion the CMP version to be stored
     */
    public void setCmpVersion(int cmpVersion) {
        this.cmpVersion = cmpVersion;

        preferences.edit().putInt(CMP_VERSION, cmpVersion).apply();
    }

    /**
     * Returns the consent screen stored in the SharedPreferences
     *
     * @return the stored consent screen
     */
    public int getConsentScreen() {
        if (consentScreen == null) {
            consentScreen = preferences.getInt(CONSENT_SCREEN, -1);
        }
        return consentScreen;
    }

    /**
     * Stores the passed consent screen in the SharedPreferences
     *
     * @param consentScreen the consent screen to be stored
     */
    public void setConsentScreen(int consentScreen) {
        this.consentScreen = consentScreen;

        preferences.edit().putInt(CONSENT_SCREEN, consentScreen).apply();
    }

    /**
     * Returns the consent language stored in the SharedPreferences
     *
     * @return the stored consent language
     */
    @Nullable
    public String getConsentLanguage() {
        if (consentLanguage == null) {
            consentLanguage = preferences.getString(CONSENT_LANGUAGE, null);
        }
        return consentLanguage;
    }

    /**
     * Stores the passed consent language in the SharedPreferences
     *
     * @param consentLanguage the consent language to be stored
     */
    public void setConsentLanguage(String consentLanguage) {
        this.consentLanguage = consentLanguage;

        preferences.edit().putString(CONSENT_LANGUAGE, consentLanguage).apply();
    }

    /**
     * Returns the vendor list version stored in the SharedPreferences
     *
     * @return the stored vendor list version
     */
    public int getVendorListVersion() {
        if (vendorListVersion == null) {
            vendorListVersion = preferences.getInt(VENDOR_LIST_VERSION, -1);
        }
        return vendorListVersion;
    }

    /**
     * Stores the passed vendor list version in the SharedPreferences
     *
     * @param vendorListVersion the vendor list version to be stored
     */
    public void setVendorListVersion(int vendorListVersion) {
        this.vendorListVersion = vendorListVersion;

        preferences.edit().putInt(VENDOR_LIST_VERSION, vendorListVersion).apply();
    }

    /**
     * Returns the max vendor id stored in the SharedPreferences
     *
     * @return the stored max vendor id
     */
    public int getMaxVendorId() {
        if (macVendorId == null) {
            macVendorId = preferences.getInt(MAX_VENDOR_ID, -1);
        }
        return macVendorId;
    }

    /**
     * Stores the passed max vendor id in the SharedPreferences
     *
     * @param macVendorId the max vendor id to be stored
     */
    public void setMaxVendorId(int macVendorId) {
        this.macVendorId = macVendorId;

        preferences.edit().putInt(MAX_VENDOR_ID, macVendorId).apply();
    }

    /**
     * Returns the vendors binary String stored in the SharedPreferences
     *
     * @return the stored vendors binary String
     */
    @NonNull
    public List<Integer> getVendors() {
        if (vendors == null) {
            vendors = PreferenceUtils.getIntegerList(preferences, VENDORS);
        }
        return vendors;
    }

    /**
     * Stores the passed vendors binary String in SharedPreferences
     *
     * @param vendors binary String to be stored
     */
    public void setVendors(@Nullable List<Integer> vendors) {
        this.vendors = vendors;

        PreferenceUtils.putIntegerList(preferences, VENDORS, vendors);
    }

    /**
     * Returns the purposes binary String stored in the SharedPreferences
     *
     * @return the stored purposes binary String
     */
    @NonNull
    public List<Integer> getPurposes() {
        if (purposes == null) {
            purposes = PreferenceUtils.getIntegerList(preferences, PURPOSES);
        }
        return purposes;
    }

    /**
     * Stores the passed purposes binary String in SharedPreferences
     *
     * @param purposes binary String to be stored
     */
    public void setPurposes(@Nullable List<Integer> purposes) {
        this.purposes = purposes;

        PreferenceUtils.putIntegerList(preferences, PURPOSES, purposes);
    }

    /**
     * Returns whether the consent was given for the passed purpose id
     *
     * @param purposeId the purpose id to check the consent for
     * @return true if consent was given, false otherwise
     */
    public boolean isPurposeConsentGivenForPurposeId(int purposeId) {
        return getPurposes().contains(purposeId);
    }

    /**
     * Returns whether the consent was given for the passed vendor id
     *
     * @param vendorId the vendor id to check the consent for
     * @return true if consent was given, false otherwise
     */
    public boolean isVendorConsentGivenForVendorId(int vendorId) {
        return getVendors().contains(vendorId);
    }

    @SuppressWarnings("FieldCanBeLocal")
    private final OnSharedPreferenceChangeListener preferenceChangeListener
            = new OnSharedPreferenceChangeListener() {
        final WeakReference<CMPStorage> weakStorage = new WeakReference<>(CMPStorage.this);

        @Override
        public void onSharedPreferenceChanged(SharedPreferences preferences, String key) {
            final CMPStorage storage = weakStorage.get();

            if (storage == null) {
                preferences.unregisterOnSharedPreferenceChangeListener(this);
                return;
            }

            switch (key) {
                case SUBJECT_TO_GDPR:
                    storage.subjectToGdpr = SubjectToGdpr.getValueForString(
                            preferences.getString(SUBJECT_TO_GDPR, null)
                    );
                    break;
                case CMP_ID:
                    storage.cmpId = preferences.getInt(CMP_ID, DEFAULT_CMP_ID);
                    break;
                case CMP_VERSION:
                    // TODO: Stored CMP version.
                    storage.cmpVersion = preferences.getInt(CMP_VERSION, -1);
                    break;
                case CONSENT_SCREEN:
                    storage.consentScreen = preferences.getInt(CONSENT_SCREEN, -1);
                    break;
                case CONSENT_LANGUAGE:
                    storage.consentLanguage = preferences.getString(CONSENT_LANGUAGE, null);
                    break;
                case VENDOR_LIST_VERSION:
                    storage.vendorListVersion = preferences.getInt(VENDOR_LIST_VERSION, -1);
                    break;
                case MAX_VENDOR_ID:
                    storage.macVendorId = preferences.getInt(MAX_VENDOR_ID, -1);
                    break;
                case VENDORS:
                    storage.vendors = PreferenceUtils.getIntegerList(preferences, VENDORS);
                    break;
                case PURPOSES:
                    storage.purposes = PreferenceUtils.getIntegerList(preferences, PURPOSES);
                    break;
            }
        }
    };

    public interface VendorListCallback {
        void onVendorListReceived(@NonNull VendorList vendorList);

        void onErrorOccurred(@NonNull Exception e);
    }
}