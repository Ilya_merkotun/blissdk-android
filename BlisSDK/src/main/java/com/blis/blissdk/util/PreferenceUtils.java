package com.blis.blissdk.util;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PreferenceUtils {
    private PreferenceUtils() {
    }

    @NonNull
    public static List<Integer> getIntegerList(@NonNull SharedPreferences preferences, @NonNull String key) {
        final Set<String> stringSet = preferences.getStringSet(key, null);
        final List<Integer> integerList = new ArrayList<>();

        if (stringSet != null) {
            for (String string : stringSet) {
                integerList.add(Integer.parseInt(string));
            }
        }

        return integerList;
    }

    public static void putIntegerList(@NonNull SharedPreferences preferences, @NonNull String key, @Nullable List<Integer> integerList) {
        final SharedPreferences.Editor editor = preferences.edit();
        if (integerList != null) {
            final Set<String> stringSet = new HashSet<>();

            for (Integer integer : integerList) {
                stringSet.add(String.valueOf(integer));
            }

            editor.putStringSet(key, stringSet);

        } else {
            editor.remove(key);
        }
        editor.apply();
    }
}
