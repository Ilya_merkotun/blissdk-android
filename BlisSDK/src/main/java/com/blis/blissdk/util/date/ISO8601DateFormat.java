package com.blis.blissdk.util.date;

import android.support.annotation.NonNull;

import java.text.FieldPosition;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ISO8601DateFormat extends SimpleDateFormat {
    private static final String ZERO_ZONE_REGEX = "Z$";
    private static final String ZERO_ZONE_REPLACEMENT = "+00:00";

    private static final String ZERO_ZONE_INVERSE_REGEX = "\\+00:00";
    private static final String ZERO_ZONE_INVERSE_REPLACEMENT = "Z";

    private static final String TIME_REGEX = "(\\d\\d):(\\d\\d)$";
    private static final String TIME_INVERSE_REGEX = "(\\d\\d)(\\d\\d)$";

    private static final String TIME_REPLACEMENT = "$1$2";
    private static final String TIME_INVERSE_REPLACEMENT = "$1:$2";

    ISO8601DateFormat(@NonNull String pattern, @NonNull Locale locale) {
        super(pattern, locale);
    }

    @Override
    public Date parse(String source) throws ParseException {
        if (source != null) {
            final String iso8601Source = source.replaceFirst(ZERO_ZONE_REGEX, ZERO_ZONE_REPLACEMENT)
                    .replaceFirst(TIME_REGEX, TIME_REPLACEMENT);
            return super.parse(iso8601Source);
        }
        return super.parse(null);
    }

    @Override
    public StringBuffer format(@NonNull Date date, @NonNull StringBuffer toAppendTo, @NonNull FieldPosition pos) {
        final String formattedString = super.format(date, toAppendTo, pos).toString()
                .replaceFirst(TIME_INVERSE_REGEX, TIME_INVERSE_REPLACEMENT)
                .replaceFirst(ZERO_ZONE_INVERSE_REGEX, ZERO_ZONE_INVERSE_REPLACEMENT);
        return new StringBuffer(formattedString);
    }
}
