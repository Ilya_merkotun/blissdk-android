package com.blis.blissdk.util.date;

import java.text.DateFormat;
import java.util.Locale;

public class DateUtils {
    private DateUtils() {
    }

    public static final DateFormat FORMAT = new ISO8601DateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault());
}
