package com.blis.blissdk.util;

import android.support.annotation.NonNull;

import com.blis.blissdk.model.Identifiable;

import java.util.ArrayList;
import java.util.List;

public class IdentifiableUtils {
    private IdentifiableUtils() {
    }

    @NonNull
    public static <T extends Identifiable> List<Integer> getIds(@NonNull List<T> identifiableList) {
        final List<Integer> list = new ArrayList<>(identifiableList.size());
        for (Identifiable identifiable : identifiableList) {
            list.add(identifiable.getId());
        }
        return list;
    }

    @NonNull
    public static <T extends Identifiable> List<T> filterItems(@NonNull List<T> items, @NonNull List<Integer> ids) {
        final List<T> filteredItems = new ArrayList<>();
        for (T item : items) {
            if (ids.contains(item.getId())) {
                filteredItems.add(item);
            }
        }
        return filteredItems;
    }
}
