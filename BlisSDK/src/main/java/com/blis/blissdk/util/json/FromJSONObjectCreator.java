package com.blis.blissdk.util.json;

import android.support.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

public interface FromJSONObjectCreator<T> {
    @NonNull
    T fromJSONObject(@NonNull final JSONObject json) throws JSONException;
}
