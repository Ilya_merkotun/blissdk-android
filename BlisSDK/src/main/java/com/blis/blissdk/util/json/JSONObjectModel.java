package com.blis.blissdk.util.json;

import android.support.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

public interface JSONObjectModel {
    @NonNull
    JSONObject toJSONObject() throws JSONException;
}
