package com.blis.blissdk.util;

import android.support.annotation.NonNull;

import com.blis.blissdk.model.vendor.VendorList;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;

public class InputStreamUtils {
    private InputStreamUtils() {
    }

    @NonNull
    public static String readStreamAsString(@NonNull InputStream inputStream) throws IOException {
        final InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        final StringBuilder builder = new StringBuilder();

        String line;

        while ((line = bufferedReader.readLine()) != null) {
            builder.append(line);
        }

        inputStream.close();

        return builder.toString();
    }

    @NonNull
    public static VendorList readVendorList(@NonNull InputStream inputStream)
            throws IOException, JSONException, ParseException {
        return new VendorList(new JSONObject(readStreamAsString(inputStream)));
    }
}
