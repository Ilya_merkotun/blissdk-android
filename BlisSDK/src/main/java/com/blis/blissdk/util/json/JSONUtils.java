package com.blis.blissdk.util.json;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class JSONUtils {

    private JSONUtils() {
    }

    @NonNull
    public static <T> List<T> list(@Nullable final JSONArray jsonArray,
                                   @NonNull final FromJSONObjectCreator<T> creator)
            throws JSONException {
        final ArrayList<T> list = new ArrayList<>();
        if (jsonArray != null) {
            for (int index = 0; index < jsonArray.length(); index++) {
                list.add(creator.fromJSONObject(jsonArray.getJSONObject(index)));
            }
        }

        return list;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public static <T> List<T> list(@Nullable final JSONArray jsonArray) throws JSONException {
        final ArrayList<T> list = new ArrayList<>();
        if (jsonArray != null) {
            for (int index = 0; index < jsonArray.length(); index++) {
                list.add((T) jsonArray.get(index));
            }
        }
        return list;
    }

    @NonNull
    public static <T> JSONArray array(@NonNull List<T> list) throws JSONException {
        final JSONArray array = new JSONArray();

        T item;
        for (int index = 0; index < list.size(); index++) {
            item = list.get(index);

            if (item instanceof JSONObjectModel) {
                array.put(index, ((JSONObjectModel) item).toJSONObject());
            } else {
                array.put(index, item);
            }
        }

        return array;
    }
}
