# BlisSDK

## Adding to your project

1. Add the library to your project in either of the following ways (if you created the library module within          the same project, then it's already there and you can skip this step):
    * Add the compiled AAR (or JAR) file (the library must be already built):
        1. Click File > New > New Module.
        1. Click Import .JAR/.AAR Package then click Next.
        1. Enter the location of the compiled AAR or JAR file then click Finish.
    * Import the library module to your project (the library source becomes part of your project):
        1. Click File > New > Import Module.
        1. Enter the location of the library module directory then click Finish. (The library module is copied to your project, so you can actually edit the library code. If you want to maintain a single version of the library code, then this is probably not what you want and you should instead add the compiled AAR file as described above.)
1. Make sure the library is listed at the top of your settings.gradle file, as shown here for a library named "BlisSDK":

```
include ':app', ':BlisSDK'
```

1. Open the app module's build.gradle file and add a new line to the dependencies block as shown in the following snippet:

```
dependencies {
    compile project(":BlisSDK")
}
    
```
 1. Click Sync Project with Gradle Files.

 1. Adding the JSON File, The BlisSDKConfig.json file is generally placed in the app/src/main/assets directory (at the root of the Android Studio app module).
```
{
    "URL" : "https://api.rc.tr.blismedia.com",
    "APIVersion" : "1",
    "CmpId" : 1,
    "CmpVersion" : 2,
    "VENDOR_LIST_URL" : "https://vendorlist.consensu.org/vendorlist.json"
}
```

Any code and resources in the Android library is now accessible to your app module, and the library AAR file is bundled into your APK at build time.

### Usage

BlisSDK is a singleton, you must take instance `BlisSDK.getInstance(appliaction)`

All activities that support locations uploading (sdk) should be annotated with @BlisActivity annotation.

```
@BlisActivity
public class SomeActivtiy extends Activity {
    ...
}
```

After the start of activity, annotated by @BlisActivity, sdk checks if current device locale country is EU country and runs GDPR process in that case.

SDK will show this GDPR dialog:

![GDPR dialog](screenshots/gdpr_dialog.png)

which will allow user to accept or manage GDPR compliance.

If user clicks on manage button the GDPR managing dialog will be shown:

![GDPR managing dialog](screenshots/gdpr_manage_dialog.png)

After user clicks on accept button or finishes GDPR managing, GDPR will be enabled and user selection will be stored into shared preference (see `CMPStorage` for more details).

**Note:** If device locale isn't EU country, GDPR process will be skipped.

After user accept GDPR, location permissions will be requested:

![GDPR managing dialog](screenshots/permissions_dialog.png)

If the user grants location permissions sdk will start working.

If you want to show GDPR dialog manually you can call `requestGDPR()` method.

```
BlisSDK.getInstance(application).requestGDPR()
```

### Change upload filters

Set minimum locations count required to make locations upload.

```
BlisSDK.getInstance(application).setCountFilter(10);
```

Set minimum time in milliseconds between locations uploads.

```
BlisSDK.getInstance(application).setUploadTimeInterval(5 * 60 * 1000);
```
### Callbacks
```
public interface BlisSDKListener {
    void finishUploadLocations(long time, List<LocationEvent> locationEvents);
    void failedUploadLocations(String errorMessage);
}
```
  